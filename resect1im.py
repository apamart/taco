#!/usr/bin/env python3
""" Script pour la résection d'une image (ou plusieurs...) """
import sys
import os
import logging
import argparse
import pathlib
from shutil import rmtree, copytree, copy
from contextlib import suppress
from lxml import etree

import config
import emulsion
import taco2 as taco

logger = logging.getLogger()

def resection_main(fpattern, image, ori, ori2, n_images=6, skip_tapioca=False, resol=None, resol_bn=None, c3dc_mode='MicMac', apericloud=None):
    """processes ONE uncalibrated image"""
    
    if apericloud is None:
        apericloud = not config.AIOLI_MODE
    
    #fpattern2  = '(%s)' % '|'.join(images)
    fpattern12 = f'({fpattern}|{image})'
    suffix = '_resect' # on met les tie-points dans un dossier Homol à part
    
    rmtree(f'Homol{suffix}', ignore_errors=True)

    try:
        emulsion.started('feature')
        if resol and resol_bn:
            res_lo, res_hi = str(resol), str(resol_bn)
        else:
            last = taco.Manioc.load_seq()
            logger.debug(last)
            res_lo, res_hi = last[-1]

        logger.info('tapioca incremental all %s', resol)
        taco.micmac('Tapioca', 'All', fpattern12, res_lo, f'PostFix={suffix}',
            'Detect=Digeo', grep=('matches', 'pts'))
        emulsion.finished()
    except taco.MicMacError as e:
        emulsion.failed()
        logger.critical(e)
        sys.exit(1)
    
    # liste des images ayant des correspondances avec la nouvelle
    top = taco.top_homol_matches(image, suffix, n_images, ext='.dat')
    logger.debug(top)
    matches = [m[0] for m in top]
    logger.info(f'top {len(matches)} images matched')
    if len(matches) == 0:
        sys.exit(1)
    files = []
    files.extend(matches)
    files.append(image)
    fpatternBN  = '(%s)' % '|'.join(files)

    # refaire une passe pleine resolution sur les matches
    bnsuffix = f'{suffix}HI'
    taco.micmac('Tapioca', 'All', image, res_hi, f'PostFix={bnsuffix}', 'Pat2=(%s)' % '|'.join(matches),
              'ExpTxt=1', 'Detect=Digeo', grep=('matches', 'pts'))

    logger.debug('antipasti')
    try:
        taco.antipasti(fpatternBN, ori2, frozen=(fpattern, ori), opt=[f'SH={bnsuffix}', 'ExpTxt=1'], ply_suffix=image)
    except:
        resect11p(image, bnsuffix, matches, n_images, apericloud, c3dc_mode)

def resect11p(image, suffix, matches, n_images, apericloud, c3dc_mode):
        logger.info('generating points %s...', image)
        im_xml_path = f'ResecIm-{image}.xml'
        ter_xml_path = f'ResecTer-{image}.xml'
        with suppress(FileNotFoundError):
            os.remove(im_xml_path)
        with suppress(FileNotFoundError):
            os.remove(ter_xml_path)
        
        with suppress(taco.MicMacError): # on ne s'arrête pas si une image rate
            for m in matches:
                # [SH,match,image] !
                taco.micmac('Im2XYZ', f'PIMs-{c3dc_mode}/Nuage-Depth-{m}.xml', im_xml_path,
                       ter_xml_path, f'PtHom=[{suffix},{m},{image}]')
            
        with open(im_xml_path, 'r') as im_xml:
            root = etree.parse(im_xml)
            mesures = root.xpath("./MesureAppuiFlottant1Im/OneMesureAF1I")
        
        # XXX focallength nécessaire pour éviter "Cannot compute association"
        logger.info('11 parameters camera space resection with %d points...', len(mesures))
        emulsion.started('resection')
        ransac_iter = 1000
        ransac_p = 0.3
        
        try:
            taco.micmac('Init11P', ter_xml_path, im_xml_path, 'FM=0', f'Rans=[{ransac_iter},{ransac_p}]')
            emulsion.finished()
        except:
            emulsion.failed()
            raise
                    
        ori_file = f'Ori-11ParamComp/Orientation-{image}.xml'
        
        if os.path.isfile(ori_file):
            # copie ori et Ori-11ParamComp dans ori2
            copy(ori_file, 'Ori-All/')
            try: # pour Aïoli...
                copy(ori_file, 'MicMac/Ori-All/')
            except:
                pass
                            
            if apericloud:
                taco.micmac(
                    'AperiCloud', image, 'All', f'Out=11p-{image}.ply', 
                    'StepIm=16', 'KeyName=NONE', 
                    #'GCPCtrl=[%s,%s,1]' % (ter_xml_path, im_xml_path)
                )
            
            logger.info('done %s', image)

def cli():
    """entry-point for cli"""
    default_cpath = pathlib.Path('.')
    if config.AIOLI_MODE:
        default_cpath = config.AIOLI_CHANTIER_DIR

    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("-c", "--chantier-path", type=pathlib.Path, default=default_cpath)
    #parser.add_argument("-m", "--manioc-level", action="count", default=0)
    parser.add_argument("resection_image", help='Image sur lequelle faire la résection (PAS DE PATTERN)')
    parser.add_argument("-p", "--resection-pattern", default='0+_.+', help='Pattern itération précédente (avec nuage dense)')
    parser.add_argument("-i", "--resection-orientation", help='Orientation itération précédente', default='All')
    parser.add_argument("-r", "--resection-resolution", type=int, default=None, help='Résolution Tapioca')
    parser.add_argument("-rn", "--neighbors-resolution", type=int, default=None, help='Résolution pour l\'extraction sur meilleurs voisins Tapioca')
    parser.add_argument("-bn", "--neighbors-number", type=int,help='Nombre max de meilleurs voisins pour la résection', default=6)
    parser.add_argument("--skip-tapioca", action='store_true')
    #parser.add_argument("--c3dc-options", type=str, nargs='+')
    if config.AIOLI_MODE:
        parser.add_argument("--process-id", help="Process ID pour Aïoli", type=emulsion.set_process_id)

    args = parser.parse_args()

    path = args.chantier_path.expanduser()
    os.chdir(str(path))
    
    logger.info('reading metadata') 
    # on lit le json resec-IMAGE.json
    # ou on le crée et on modifie les métadonnées si besoin
    #for image in args.resection_image:
    #    taco.process_metadata('resec-%s.json' % image, path=image)
    #    if config.AIOLI_MODE: # pour Violette / metadataParser.php, à gérer directement ?
    #        taco.micmac('MMXmlXif', image, suppress_error=True) # MMXmlXif renvoie 1...
        
    fp = args.resection_pattern 
    ori = args.resection_orientation
    ori2 = 'Resec'
    logger.info(f'resection {args.resection_image} ({ori2}) to {fp} ({ori})')
    resection_main(fp, args.resection_image, ori, ori2, 
        skip_tapioca=args.skip_tapioca, n_images=args.neighbors_number,
        resol=args.resection_resolution, resol_bn=args.neighbors_resolution, apericloud=(not config.AIOLI_MODE))

    if config.AIOLI_MODE:
        # pour indexation Adeline
        for image in args.resection_image:
            taco.micmac('GenerateBorderCam', f'Ori-All/Orientation-{image}.xml', '1')
    
    # cleanup
    rmtree("NewOriTmpQuick", ignore_errors=True)
    rmtree("Ori-InterneScan", ignore_errors=True)

if __name__ == "__main__":
    taco.configure_log_and_errors(logger, 'taco-resection')
    cli()

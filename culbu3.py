#!/usr/bin/env python3
""" Mise à l'échelle avec QR code via GCP MicMac
Implémentation culbuto avec pyzbar (compatible python3) et ThreadPool"""

# TODO utiliser logging
# import logging

import sys
from PIL import Image
from glob import glob
from multiprocessing.dummy import Pool as ThreadPool
from multiprocessing import cpu_count
import collections
import itertools
from lxml import etree
import logging
from pathlib import Path
from typing import List, Dict, Tuple

## MONKEYPATCH PYZBAR
## decode renvoie (data, location)
## avec location dans le bon ordre
from pyzbar import pyzbar
from ctypes import string_at
from pyzbar.wrapper import (
    zbar_symbol_get_data, ZBarSymbol,
    zbar_symbol_get_loc_size, zbar_symbol_get_loc_x, zbar_symbol_get_loc_y,
)

def _decode_symbols(symbols):
    for symbol in symbols:
        data = tuple(string_at(zbar_symbol_get_data(symbol)).decode().split())
        symbol_type = ZBarSymbol(symbol.contents.type).name
        location = [
            (
                zbar_symbol_get_loc_x(symbol, index),
                zbar_symbol_get_loc_y(symbol, index)
            )
            for index in range(zbar_symbol_get_loc_size(symbol))
        ]
        yield (data, location)
        
pyzbar._decode_symbols = _decode_symbols
## FIN MONKEYPATCH PYZBAR

logger = logging.getLogger(__name__)

def process_image(path):
    """ouvre l'image path 
       renvoie une liste de tuple (path, data, location) 
       pour chaque QR code dans l'image
    """
    # on a besoin de _decode_symbols modifié pour le bon ordre
    return pyzbar.decode(Image.open(path), symbols=[ZBarSymbol.QRCODE])

def scan_all_images(paths, num_workers=None):
    """scan toutes les images renvoie un dictionnaire results
     à une image est associée une _liste_ de detection
     image : nom de l'image
     detection : tuple (axes, location)
         axes : tuple de chaînes (contenu du QR code splitté selon blancs)
         location : liste des (x, y) dans l'ordre top left, bottom left, bottom right, top right
    """
    if num_workers is None:
        num_workers = cpu_count()
    # on scanne les images en parallèle
    pool = ThreadPool(num_workers)
    ret = pool.map(process_image, paths)
    images = (Path(path).name for path in paths) # on récupère seulement le nom

    return dict(zip(images, ret))

def xml_3d_oneappuisdaf(nom, coord3d):
    el_pt = etree.Element("OneAppuisDAF")
    el_coord = etree.SubElement(el_pt, "Pt")
    el_nom = etree.SubElement(el_pt, "NamePt")
    el_incert = etree.SubElement(el_pt, "Incertitude")
    el_nom.text, el_coord.text, el_incert.text = nom, coord3d, '0.1 0.1 0.1'
    
    return el_pt

def xml_2d_onemesureaf1i(nom, coord2d):
    el_pt = etree.Element("OneMesureAF1I")	
    el_nom = etree.SubElement(el_pt, "NamePt")
    el_coord = etree.SubElement(el_pt, "PtIm")
    el_nom.text, el_coord.text = nom, coord2d
    
    return el_pt

def culbuto(image_path_list, xml_2d_path, xml_3d_path, x_override=None, y_override=None, num_workers=None):
    """prend une liste de chemins d'images
       crée les xml pour MicMac GCPBascule qui vont bien : xml_2d_path, xml_3d_path
       x_override et y_override valeur entières pour forcer valeur axes (cas 3 qr codes)
    """
    
    results = scan_all_images(image_path_list, num_workers)

    if not results:
        raise Exception # pas de detections
    
    print(results)
    
    # on lit un code dans les résultats
    # c'est lui qui définira quel type de code on va traiter
    code = None
    try:
        for v in results.values():
            if not v:
                continue
            print(v)
            first_axes, _ = v[0]
    except:
        raise # TODO
    
    
    if len(first_axes) == 1:
        # cas code à une valeur (unité)
        # la valeur est la largeur métrique du qr code physique
        unit = int(first_axes[0])

        for image, detections in results.items():
            if len(detections) > 1:
                raise Exception # plusieurs codes par image..
            axes, location = detections[0]
            if len(axes) != 1:
                raise Exception # plusieurs valeurs ou aucune dans un code..
            if unit != int(axes[0]):
                raise Exception # existe un code aux valeurs différentes..
        unit = unit * 100 # facteur
        data_3d = [
            ('TopLeft', f'0 {unit} 0'),
            ('BottomLeft', '0 0 0'),
            ('BottomRight', f'{unit} 0 0'),
            ('TopRight', f'{unit} {unit} 0'),
        ]
    elif len(first_axes) == 2:
        # cas codes à deux valeurs (x y)
        # 3 codes nécessaire (origine, axe x, axe y)
        axes_to_images = collections.defaultdict(list)
        for image, detections in results.items():
            if len(detections) > 3:
                raise Exception # plus de trois codes sur une même image..
            for axes, location in detections:
                if len(axes) != 2:
                    raise Exception # un code ne contient pas exactement 2 infos..
                axes_to_images[axes].append(image)
        unique_axes = axes_to_images.keys()
        if len(unique_axes) != 3:
            raise Exception # plus de trois codes différents au total..
        
        for image, detections in axes_to_images.items():
            if len(detections) < 3:
                raise Exception # point présent sur moins de 3 images 
                # eventuellement 2 images suffisent a tester
        
        data_3d = [] # on remplit data_3d
        for x, y in unique_axes: # on cherche l'origine
            if (x, y) == ('0', '0'):
                data_3d.append(('O', '0 0 0'))
                break
        for x, y in unique_axes: # on cherche X
            if x != '0' and y == '0':
                if x_override is None:
                    if x == 'x' or x == 'X':
                        raise Exception # préciser valeur X ..
                    else:
                        x_value = int(x)
                else:
                    x_value = x_override
                x_value = x_value * 100 # facteur
                data_3d.append(('X', f'{x_value} 0 0'))
                break
        for x, y in unique_axes: # on cherche Y
            if x == '0' and y != '0':
                if y_override is None:
                    if y == 'y' or y == 'Y':
                        raise Exception # préciser valeur Y ..
                    else:
                        y_value = int(y)
                else:
                    y_value = y_override
                y_value = y_value * 100 # facteur
                data_3d.append(('Y', f'0 {y_value} 0'))
                break

        if len(data_3d) != 3:
            raise Exception # pas détecté les 3 codes différents nécessaires..
    
    else: # ni 1 ni 2 valeurs ..
        raise Exception # format de valeurs inconnu..
    
    # on crée les xml à partir de data_3d et data_2d
    daf = etree.Element("DicoAppuisFlottant")
    daf.extend(xml_3d_oneappuisdaf(name, coord) for name, coord in data_3d)
    maf = etree.Element("SetOfMesureAppuisFlottants")
    
    for im, codes in results.items():
        mesureappui = etree.Element("MesureAppuiFlottant1Im")
        imagenom = etree.SubElement(mesureappui, "NameIm")
        imagenom.text = im

        if len(first_axes) == 1:
            values, location = codes[0] # on considère un seul code
            mesures = {
                'TopLeft': '%d %d' % location[0],
                'BottomLeft': '%d %d' % location[1],
                'BottomRight': '%d %d' % location[2],
                'TopRight': '%d %d' % location[3],
            }
        elif len(first_axes) == 2:
            mesures = {}
            for values, location in codes:
                x, y = values
                if (x, y) == ('0', '0'):
                    mesures['O'] = '%d %d' % location[1]
                elif x == '0':
                    mesures['Y'] = '%d %d' % location[0]
                elif y == '0':
                    mesures['X'] = '%d %d' % location[2]

        if len(mesures) != 0:
            mesureappui.extend(xml_2d_onemesureaf1i(name, coord) for name, coord in mesures.items())
            maf.append(mesureappui)

    with open(xml_3d_path, "wb") as f:
        xml_3d = etree.tostring(daf, pretty_print=True, xml_declaration=True, encoding='UTF-8')
        f.write(xml_3d)
    with open(xml_2d_path, "wb") as f:
        xml_2d = etree.tostring(maf, pretty_print=True, xml_declaration=True, encoding='UTF-8')
        f.write(xml_2d)

if __name__ == "__main__":
    culbuto(glob("*.JPG"), "Culbuto-S2D.xml", "Culbuto-S3D.xml")

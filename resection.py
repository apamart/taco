#!/usr/bin/env python3
""" Script pour la résection d'une image (ou plusieurs...) """
import sys
import os
import logging
import argparse
import pathlib
from shutil import rmtree, copytree, copy
from contextlib import suppress
from lxml import etree

import config
import emulsion
import taco2 as taco

logger = logging.getLogger()

def resection_main(fpattern, images, ori, ori2, n_images=6, skip_tapioca=False, resol=None, c3dc_mode='MicMac', apericloud=None):
    """processes one uncalibrated image"""
    
    if apericloud is None:
        apericloud = not config.AIOLI_MODE
    
    fpattern2  = '(%s)' % '|'.join(images)
    fpattern12 = '(%s|%s)' % (fpattern, fpattern2)
    suffix = '_resect' # on met les tie-points dans un dossier Homol à part
    
    for image in images:
        if not skip_tapioca:
            rmtree('Homol%s' % suffix, ignore_errors=True)

            try:
                emulsion.started('feature')
                if not resol:
                    res_lo, res_hi = taco.Manioc.load_seq()[-1]
                    resol = str(res_hi)

                logger.info('tapioca incremental all %s', resol)
                taco.micmac('Tapioca', 'All', fpattern, resol, 'PostFix=%s' % suffix, 'Pat2=%s' % image,
                       'ExpTxt=1', 'Detect=Digeo', grep=('matches', 'pts'))
                emulsion.finished()
            except taco.MicMacError as e:
                emulsion.failed()
                logger.critical(e)
                sys.exit(1)
        
        # liste des images ayant des correspondances avec la nouvelle
        logger.info('generating points %s...' % image)
        im_xml_path = 'ResecIm-%s.xml' % image
        ter_xml_path = 'ResecTer-%s.xml' % image
        with suppress(FileNotFoundError):
            os.remove(im_xml_path)
        with suppress(FileNotFoundError):
            os.remove(ter_xml_path)
    
        matches = [m[0] for m in taco.top_homol_matches(image, suffix, n_images)]
        logger.info('top %d images matched', len(matches))
        if len(matches) == 0:
            sys.exit(1)
        
        # refaire une passe pleine résolution sur les matches
        #suffix = '%sHI' % suffix
        #micmac('Tapioca', 'All', image, '-1', 'PostFix=%s' % suffix, 'Pat2=(%s)' % '|'.join(matches),
        #           'ExpTxt=1', 'Detect=Digeo', grep=('matches', 'pts'))
        
        with suppress(taco.MicMacError): # on ne s'arrête pas si une image rate
            for m in matches:
                # [SH,match,image] !
                taco.micmac('Im2XYZ', 'PIMs-%s/Nuage-Depth-%s.xml' % (c3dc_mode, m), im_xml_path,
                       ter_xml_path, 'PtHom=[%s,%s,%s]' % (suffix, m, image), nolog=True)
            
            with open(im_xml_path, 'r') as im_xml:
                root = etree.parse(im_xml)
                mesures = root.xpath("./MesureAppuiFlottant1Im/OneMesureAF1I")
            
            # XXX focallength nécessaire pour éviter "Cannot compute association"
            logger.info('11 parameters camera space resection with %d points...', len(mesures))
            emulsion.started('resection')
            ransac_iter = 1000
            ransac_p = 0.3
            
            try:
                taco.micmac('Init11P', ter_xml_path, im_xml_path, 'FM=0', 'Rans=[%d,%f]' % (ransac_iter, ransac_p))
                emulsion.finished()
            except:
                emulsion.failed()
                raise
                        
            ori_file = 'Ori-11ParamComp/Orientation-%s.xml' % image
            
            if os.path.isfile(ori_file):
                # copie ori et Ori-11ParamComp dans ori2
                copy(ori_file, 'Ori-All/')
                try: # pour Aïoli...
                    copy(ori_file, 'MicMac/Ori-All/')
                except:
                    pass
                                
                if apericloud:
                    taco.micmac(
                        'AperiCloud', image, 'All', 'Out=11p-%s.ply' % image, 
                        'StepIm=16', 'KeyName=NONE', 
                        #'GCPCtrl=[%s,%s,1]' % (ter_xml_path, im_xml_path)
                    )
                
                logger.info('done %s' % image)

def cli():
    """entry-point for cli"""
    default_cpath = '.'
    if config.AIOLI_MODE:
        default_cpath = config.AIOLI_CHANTIER_DIR

    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("-c", "--chantier-path", type=pathlib.Path, default=default_cpath)
    #parser.add_argument("-m", "--manioc-level", action="count", default=0)
    parser.add_argument("resection_image", nargs='+', help='Image(s) sur lequelle faire la résection (PAS DE PATTERN)')
    parser.add_argument("-rp", "--resection-pattern", default='ITER0+__.+', help='Pattern itération précédente (avec nuage dense)')
    parser.add_argument("-rs", "--resection-resolution", default=None, help='Résolution Tapioca')
    parser.add_argument("--skip-tapioca", action='store_true')
    #parser.add_argument("--c3dc-options", type=str, nargs='+')
    parser.add_argument("--process-id", help="Process ID pour Aïoli", type=emulsion.set_process_id)

    args = parser.parse_args()

    path = args.chantier_path.expanduser()
    os.chdir(str(path))
    
    logger.info('reading metadata') 
    # on lit le json resec-IMAGE.json
    # ou on le crée et on modifie les métadonnées si besoin
    for image in args.resection_image:
        taco.process_metadata('resec-%s.json' % image, path=image)
        if config.AIOLI_MODE: # pour Violette / metadataParser.php, à gérer directement ?
            taco.micmac('MMXmlXif', image, suppress_error=True) # MMXmlXif renvoie 1...
        
    fp = args.resection_pattern 
    ori = 'All'
    ori2 = 'Resec'
    logger.info('resection %s (%s) to %s (%s)' % (args.resection_image, ori2, fp, ori))
    resection_main(fp, args.resection_image, ori, ori2, 
        skip_tapioca=args.skip_tapioca, 
        resol=args.resection_resolution, apericloud=(not config.AIOLI_MODE))

    if config.AIOLI_MODE:
        # pour viewer ND
        taco.micmac('OriExport', 'Ori-All/Orientation-.*.xml', 'export.txt')
        # pour indexation Adeline
        for image in args.resection_image:
            taco.micmac('GenerateBorderCam', 'Ori-All/Orientation-%s.xml' % image, '1')
    
    # cleanup
    rmtree("NewOriTmpQuick", ignore_errors=True)
    rmtree("Ori-InterneScan", ignore_errors=True)

if __name__ == "__main__":
    taco.configure_log_and_errors(logger, 'taco-resection')
    cli()

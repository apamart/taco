#!/usr/bin/env python3
""" Script pour les itérations incrémentales par bloc"""
import sys
import os
import logging
import argparse
import pathlib
from shutil import rmtree, copytree, copy
#import regex

import config
import emulsion
import taco2 as taco

logger = logging.getLogger()

def incremental_main(fpattern, fpattern2, ori, ori2, skip_tapioca=False, run_culbuto=False):
    """incremental process"""

    fpattern12 = f'({fpattern}|{fpattern2})'
    #ori = 'All2'
    
    if run_culbuto:
        logger.info('running culbuto...')
        try:
            emulsion.started('culbuto_scan')
            culbuto(image_list, 'Culbuto-S2D.xml', 'Culbuto-S3D.xml')
            emulsion.finished()
        except:
            emulsion.failed()

    if not skip_tapioca:
        res_lo, res_hi = taco.Manioc.load_seq()[-1]
        
        logger.debug('tapioca incremental mulscale')
        try:
            emulsion.started('feature')
            taco.micmac('Tapioca', 'MulScale', fpattern2, res_lo, res_hi,
                   'Detect=Digeo', grep=('matches', 'pts'))
            taco.micmac('Tapioca', 'MulScale', fpattern, res_lo, res_hi,
                   'Detect=Digeo', f'Pat2={fpattern2}', grep=('matches', 'pts'))
            taco.micmac('Tapioca', 'MulScale', fpattern12, res_lo, res_hi, 'DLR=0',
                   'Detect=Digeo', grep=('matches', 'pts'))
            emulsion.finished()
        except:
            emulsion.failed()
            raise
        
    logger.debug('antipasti')
    taco.antipasti(fpattern12, ori2, frozen=(fpattern, ori), run_culbuto=run_culbuto)

    logger.info('done!')

def cli():
    """entry-point for cli"""
    default_cpath = '.'
    if config.AIOLI_MODE:
        default_cpath = config.AIOLI_CHANTIER_DIR

    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("-c", "--chantier-path", type=pathlib.Path, default=default_cpath,
        help="Chemin chantier (! défaut config.AIOLI_MODE: config.AIOLI_CHANTIER_DIR)")
    parser.add_argument('pattern_iter0', help="Pattern itération précédente")
    parser.add_argument('pattern_iter1', help="Pattern nouvelle itération")
    parser.add_argument('ori_iter0', nargs='?', default='All', help="Dossier Ori itération précédente")
    parser.add_argument('ori_iter1', nargs='?', default='All2', help="Dossier Ori nouvelle itération")
    
    parser.add_argument("--skip-tapioca", action='store_true')
    parser.add_argument("--culbuto", action='store_true')
    parser.add_argument("--process-id", help="Process ID pour Aïoli", type=emulsion.set_process_id)
    #parser.add_argument("--c3dc-options", type=str, nargs='+')

    args = parser.parse_args()

    path = args.chantier_path.expanduser()
    os.chdir(str(path))
    
    logger.debug('reading metadata...')
    # ici on crée un _nouveau_ (!) fichier incX.json 
    # et on modifie les métadonnées s'il faut
    # XXX comment permettre de refaire directement la dernière iter ?
    #json_file = taco.get_increment_filename('inc', ext='.json')
    #dump = taco.process_metadata(json_file)

    fp = args.pattern_iter0
    fp2 = args.pattern_iter1
    ori = args.ori_iter0
    ori2 = args.ori_iter1
    logger.debug(f'incremental {fp2} ({ori2}), on {fp} ({ori})')
    incremental_main(fp, fp2, ori, ori2, skip_tapioca=args.skip_tapioca, run_culbuto=args.culbuto)
    
    if config.AIOLI_MODE:
        # pour Adeline XXX quel Ori ? All2
        for orientation in path.glob(f'Ori-{ori2}/Orientation-*.xml'):
            taco.micmac('GenerateBorderCam', str(orientation), '1')
    
    # cleanup
    rmtree("NewOriTmpQuick", ignore_errors=True)
    rmtree("Ori-InterneScan", ignore_errors=True)

if __name__ == "__main__":
    taco.configure_log_and_errors(logger, 'taco-incremental')
    cli()

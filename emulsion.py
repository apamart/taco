""" 
Transmission de la progression (milestones) à Aïoli

- désolé pour le nom cute
- module avec globales ...
- une seule étape à la fois
- a besoin de config.AIOLI_MILESTONE_URL
- et d'être initialisé après import avec set_process_id(id)

"""
import config
import urllib.parse
import urllib.request
import ssl # insecure context
import logging

logger = logging.getLogger(__name__)

PROCESS_ID = None

current_step = None

class StatusCode:
    PROCESSING = 102
    OK = 200
    ERROR = 400
    WTF = 500

def set_process_id(pid):
    global PROCESS_ID
    PROCESS_ID = pid

def post(step, status):
    if not config.AIOLI_MODE or not config.EMULSION_ENABLED:
        #logger.debug('\033[91m %s %s \033[0m', step, status)
        return
        
        
    global PROCESS_ID
    
    if not PROCESS_ID:
        logger.error('missing process_id')
        return
        
    process_id = PROCESS_ID
    
    # insecure context
    ctx = ssl.create_default_context()
    ctx.check_hostname = False
    ctx.verify_mode = ssl.CERT_NONE
    
    url = config.AIOLI_MILESTONE_URL
    values = {'process_id' : process_id,
              'step' : step,
              'status' : status }
    headers = {'Content-Type': 'application/json'}

    data = urllib.parse.urlencode(values)
    req = urllib.request.Request(url, data, headers)
    response = urllib.request.urlopen(req)
    res_data = response.read()

def started(step):
    global current_step
    if current_step is not None:
        logger.error('already in a step!')
        post('wtf', StatusCode.WTF)
    else:
        current_step = step
        post(step, StatusCode.PROCESSING)

def finished():
    global current_step
    if current_step is None:
        logger.error('not in a step!')
        post('wtf', StatusCode.WTF)
    else:
        post(current_step, StatusCode.OK)
        current_step = None
 
def failed(critical=False):
    global current_step
    if current_step is None and not critical:
        logger.error('not in a step!')
        post('wtf', StatusCode.WTF)
    elif current_step is not None:
        post(current_step, StatusCode.ERROR)
        current_step = None
        

#import json # TODO

AIOLI_MODE = True  # pas très explicite comme config...
UNDISTORT_IMAGES = True
MM3D_BIN = '/culture3d/bin/mm3d'
LOG_DIR = '/chantier/Log/'
AIOLI_ACQUISITION_DIR = '/acq'
AIOLI_CHANTIER_DIR = '/chantier'
AIOLI_MILESTONE_URL = 'https://aiolidev.map.cnrs.fr/_proc/milestone'
EMULSION_ENABLED = False # transmission d'erreurs

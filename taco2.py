#!/usr/bin/env python3

import logging
import os
import sys
import json
import itertools
import traceback
import argparse
import subprocess
import pathlib
from culbu3 import culbuto
from subprocess import Popen, PIPE, STDOUT, DEVNULL
from contextlib import ExitStack, suppress
from shutil import rmtree, copytree, copy
from functools import partial
from lxml import etree
#from psutil import virtual_memory

import config
import emulsion

logger = logging.getLogger(__name__)

def log_uncaught_exceptions(ex_cls, ex, tb):
    """logs exceptions with traceback..."""
    emulsion.failed(critical=True)
    logger.critical(''.join(traceback.format_tb(tb)))
    logger.critical('{0}: {1}'.format(ex_cls, ex))

def configure_log_and_errors(logger, logname=None, level=logging.DEBUG):
    """configure logging module"""

    log_formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
    logger.setLevel(level)
    
    # in console
    stream_handler = logging.StreamHandler()
    stream_handler.setFormatter(log_formatter)
    stream_handler.setLevel(level)
    logger.addHandler(stream_handler)
    
    # in file
    if logname is not None:
        log_filename = get_log_filename(logname)
        file_handler = logging.FileHandler(log_filename)
        file_handler.setFormatter(log_formatter)
        file_handler.setLevel(level)
        logger.addHandler(file_handler)

    sys.excepthook = log_uncaught_exceptions

class Error(Exception):
    """base class for errors"""
    pass

class MicMacError(Error):
    """micmac execution errors"""
    def __init__(self, ret, args, msg):
        self.ret = ret
        self.args = args
        self.msg = msg
    def __str__(self):
        return ('return: %s; args: %s; msg: %s' 
                % (self.ret, ' '.join(self.args), ''.join(self.msg)))

def get_increment_filename(base, ext=''):
    """returns `base`, or if file `base` in cwd exists, `base`.0, and so on"""
    name = f'{base}{ext}'
    for i in itertools.count(0):
        if not os.path.exists(name):
            break
        name = f'{base}.{i}{ext}'
    return name

def get_log_filename(name):
    """returns a new log filename from `name`"""
    logdir = config.LOG_DIR
    if not os.path.isdir(logdir):
        os.makedirs(logdir)
    
    return get_increment_filename(f'{logdir}{name}', '.log')

def micmac(*args, grep=None, nolog=False, suppress_error=False):
    """launches mm3d with arguments `args`
       logs in `args[0]`.log or eventually `args[0]`-`args[1]`.log... 

    Args:
        args: list of arguments passed to mm3d
        grep: (grep_str, grep_log) pair
                 prints in `grep_log` lines of mm3d output matching `grep_str`
    """
    name = args[0]
    if name in ('Tapioca', 'Tapas', 'C3DC', 'TestLib'):
        name = f'{name}-{args[1]}' # inclure sous commande
    cmd = [config.MM3D_BIN, *args, '@ExitOnBrkp', '@ExitOnWarn', '@ExitOnNan']
    cmdlog = ' '.join(cmd)
    err = False
    errlines = []

    logger.debug(cmdlog)
    mm3d = Popen(cmd, stdout=PIPE, stderr=STDOUT, stdin=PIPE,
                 universal_newlines=True, bufsize=1) #text-mode, line-buffered

    grep_log = ExitStack()
    if grep:
        grep_str, grep_name = grep
        grep_logname = get_log_filename(f'{name}-{grep_name}')
        grep_log = open(grep_logname, 'w')

    log = ExitStack()
    if not nolog:
        logname = get_log_filename(name)
        log = open(logname, 'w')

    with log, grep_log:
        if not nolog:
            print(cmdlog, file=log)
        if grep:
            print(cmdlog, file=grep_log)
        for line in mm3d.stdout:
            if not nolog:
                log.write(line)
            if grep and grep[0] in line:
                print(line, end='')
                grep_log.write(line)
            if "Sorry" in line or "FAIL" in line or "Error" in line:
                err = True
            if ("(press enter)" in line or "tape enter" in line) and not mm3d.poll():
                mm3d.stdin.write('\n')
            if err:
                errlines.append(line)
    if mm3d.wait() != 0 and suppress_error is False:
        raise MicMacError(mm3d.returncode, mm3d.args, errlines)

class Manioc(object):
    """classe pour mise en correspondance INITIALE avec Tapioca"""
    def __init__(self, fpattern, res_full, manioc_level=0):
        """constructeur ...
        
        Args:
            fpattern: file pattern of input photos
            res_full: largest dimension of input photos for multiscale search
                      (default None and `seq` must not be empty) 
            manioc_level: skips dimension pairs in multiscale search"""
        self.fpattern = fpattern
        self.seq = self.create_res_seq(res_full, manioc_level)
    
    @staticmethod
    def create_res_seq(res_full, manioc_level=0):
        """returns a list of (low dimension, high dimension) pairs 
           for Tapioca MulScale
        """
        increment = 0
        res = (500, -1)
        if res_full > 1000:
            increment = (res_full - 1000) // 3
            res = (500, 1000, -1)
            if increment >= 300:
                res = (500, 1000, 1000 + increment, 1000 + 2 * increment)

        seq = [(res[i], res[i+1]) for i in range(len(res) - 1)]
        seq.reverse()
        #seq = [[500, 1000+increment]]
        
        for _ in range(manioc_level):
            seq.pop()

        return seq
    
    @staticmethod
    def load_seq():
        """returns last (low, hi) resolution tuple used by manioc/tapioca"""
        with open('resolution-tapioca.json') as tr:
            return json.load(tr, parse_int=str)
    
    def next(self):
        """retire de la liste des résolutions la dernière essayée"""
        self.seq.pop()
    
    def save_seq(self):
        with open('resolution-tapioca.json', 'w+') as tr:
            json.dump(self.seq, tr)
    
    def run(self):
        """mulscale initial tie-points detection"""
        if not self.seq:
            raise IndexError
        logger.info('feature extraction and matching...')
        
        
        while True:
            try:
                res_lo, res_hi = self.seq[-1]
                logger.info(f'res_lo={res_lo} res_hi={res_hi}')
                emulsion.started('feature')
                micmac('Tapioca', 'MulScale', self.fpattern, str(res_lo), str(res_hi),
                    #'@SFS',
                    'Detect=Digeo', grep=('matches', 'pts'))
                self.save_seq()
                
                #rmtree("HomolUnfiltered", ignore_errors=True)
                #micmac('HomolFilterMasq', fpattern, 'DistId=1')
                #os.rename('Homol', 'HomolUnfiltered')
                #os.rename('HomolMasqFiltered', 'Homol')
                emulsion.finished()
                break
            except MicMacError:
                emulsion.failed()
                self.next() # resolution suivante
            except:
                emulsion.failed()
                raise

def tapas(fpattern, ori='All', model='FraserBasic', martini=False, refine=True,
                 #ratafia=False, 
                 frozen=False, use_cal=False, opt=None):
    """launches one parametrized Tapas

    Args:
        fpattern: file pattern of input photos
        ori: folder for output orientations
        model: model to use
        martini: if True initialises orientations with Martini
        refine: if False adds RefineAll=0 (default True)
        frozen: for incremental process, tuple (`pattern`, `ori`)
                   `pattern` of already oriented photos
                   `ori` existing folder of orientations
        use_cal: takes a list of images to precalibrate
    """
    cmd = ['Tapas', model, fpattern, f'Out={ori}']
    logstr = ['calibration with', model]

    if frozen:
        # XXX recup model iter n
        frozen_pattern, frozen_ori = frozen
        #cmd.append('InCal=%s' % frozen_ori) # XXX iter n
        cmd.append(f'InOri={frozen_ori}') # XXX iter n
        cmd.append(f'FrozenPoses={frozen_pattern}')
        cmd.append(f'FrozenCalibs={frozen_pattern}')
        #cmd.append('Out=All2') # XXX iter n-1
        # XXX All ne contient que les images du pattern!
        logstr.insert(0, 'incremental')
    elif martini:
        logger.info('orientation initialisation...')
        emulsion.started('initori')
        if use_cal:
            micmac('Martini', fpattern, 'ModeNO=StdNoTTK', 'OriCalib=Cal', suppress_error=True)
            cmd.append('InCal=Cal')
            logstr.append('with precalibration')
            cmd.append('InOri=MartiniCal')
        else:
            micmac('Martini', fpattern, 'ModeNO=StdNoTTK', suppress_error=True)
            cmd.append('InOri=Martini')
        emulsion.finished()
        #if ratafia:
        #    logstr.append('ratafia')
        #    micmac('Ratafia', fpattern, suppress_error=True)
        #    cmd.append('SH=-Ratafia')
    else:
        logstr.append('no-martini')

    cmd.extend(opt or [])
    
    if not refine:
        cmd.append('RefineAll=0')
        logstr.append('RefineAll=0')

    logger.info(' '.join(logstr))
    try:
        emulsion.started('precal' if ori == 'Cal' else 'ori')
        micmac(*cmd, grep=('Residual', 'residu'))
        emulsion.finished()
    except:
        emulsion.failed()
        raise

def apericloud_cam(fpattern, ori_folder, ply_suffix=None, opt=None):
    opt = opt or []
    emulsion.started('sparse')
    logger.info('sparse 3d cloud reconstruction...')
    try:
        micmac('AperiCloud', fpattern, ori_folder, 'SeuilEc=2', *opt,
           'WithCam=0', f'Out=AperiCloud_{ori_folder}{ply_suffix or ""}.ply')
        micmac('AperiCloud', fpattern, ori_folder, 'WithPoints=0', *opt,
           f'Out=AperiCAM_{ori_folder}{ply_suffix or ""}.ply')

        emulsion.finished()
    except:
        emulsion.failed()
        raise

def antipasti(fpattern, ori_folder, fisheye=False, frozen=False, run_culbuto=False, do_c3dc=None, manioc=None, opt=None, ply_suffix=None):
    """bundle block adjustment - calibration and sparse cloud reconstruction

    Args:
        fpattern: input photos file pattern
        ori_folder: output orientations folder (without Ori- ...)
        fisheye: if True uses a fisheye model 
        frozen: for incremental process, 
                   file pattern of already computed orientations orientation
                   (disables fisheye...)
    """
    opt = opt or []
    mon_tapas = partial(tapas, fpattern, ori_folder, frozen=frozen)
    if frozen:
        recettes = [
            dict(model='FraserBasic'),
            dict(model='FraserBasic', refine=False),
            #dict(model='RadialExtended'),
            #dict(model='RadialExtended', refine=False),
            dict(model='RadialStd'),
            dict(model='RadialStd', refine=False),
        ]
    elif fisheye:
        logger.debug('fisheye')
        # TODO seulement si grand dataset? 
        # TODO si differentes focales fisheye calib sur chaque...
        fisheyefp = '(%s)' % '|'.join(fisheye[:10])
        logger.info('auto-precalibration')
        try:
            tapas(fisheyefp, ori='Cal', model='FishEyeBasic')
        except MicMacError:
            with suppress(IndexError):
                manioc(fpattern)
        
        recettes = [
            #dict(model='FishEyeEqui', martini=True),
            #dict(model='FishEyeEqui'),
            dict(model='FishEyeBasic', martini=True, use_cal=True),
            dict(model='FishEyeBasic', use_cal=True),
            dict(model='FishEyeBasic'),
            #dict(model='FishEyeEqui', refine=False),
            dict(model='FishEyeBasic', refine=False),
        ]
    else:
        recettes = [
            dict(model='FraserBasic', martini=True),
            dict(model='FraserBasic'),
            dict(model='FraserBasic', refine=False),
            #dict(model='RadialExtended'),
            #dict(model='RadialExtended', refine=False),
            dict(model='RadialStd'),
            dict(model='RadialStd', refine=False),
        ]
    recettes.reverse()
    
    while recettes:
        try:
            recette = recettes[-1]
            mon_tapas(**recette, opt=opt)
            if run_culbuto:
                culbuto_bascule(fpattern, ori_folder, 'Culbuto-S2D.xml', 'Culbuto-S3D.xml')
            
            apericloud_cam(fpattern, ori_folder, ply_suffix=ply_suffix, opt=opt)
            
            if config.UNDISTORT_IMAGES:
                undistort_images(fpattern, ori_folder)
            if do_c3dc:
                c3dc(fpattern, ori_folder, ply_suffix=ply_suffix)
            return
        except MicMacError:
            if not frozen: # XXX non nécessaire ?
                # autant qu'on peut, augmenter manioc et reessayer
                with suppress(IndexError):
                    if manioc:
                        manioc.next()
                        manioc.run()
                        continue
            recettes.pop() # modele suivant
        except IndexError: # XXX dans quel cas?
            logger.error('adjustment failed')
            sys.exit(1)
    
    raise Exception # aucune recette
    
def culbuto_bascule(fpattern, ori, s2d, s3d):
    """if s2d and s3d files exists (created by culbuto) do orientation bascule"""
    if not os.path.isfile(s2d) or not os.path.isfile(s3d):
        return
    logger.info('orientation bascule')
    ori_pre = f'{ori}-preculbuto'
    if os.path.exists(f'Ori-{ori_pre}'):
        rmtree(f'Ori-{ori_pre}')
    os.rename(f'Ori-{ori}', f'Ori-{ori_pre}')

    try:
        emulsion.started('culbuto_bascule')
        micmac('GCPBascule', fpattern, ori_pre, ori, s3d, s2d)
    except MicMacError:
        logger.error('bascule error')
        emulsion.failed()
    else:
        rmtree(f'Ori-{ori_pre}')
        emulsion.finished()

def c3dc(fpattern, ori, mode='MicMac', zoomf=None, ply_suffix=None):
    """do C3DC MicMac"""
    logger.info('dense 3d cloud reconstruction...')
    emulsion.started("c3dc")
    options = []
    if zoomf:
        options.append(f'ZoomF={zoomf}')
    try:
        micmac('C3DC', mode, fpattern, ori, *options, grep=('Chx', 'score'))
        emulsion.finished()
    except:
        emulsion.failed()
        raise
    
def undistort_images(fpattern, ori):
    logger.info('undistort images...')
    try:
        emulsion.started('undist')
        micmac('Drunk', fpattern, ori, 'Out=Drunk/')
        emulsion.finished()
    except:
        emulsion.failed()
        raise

def process_metadata(json_file, path=None, auto_fix=True):
    """returns array of metadata dict, either from an existing json_file, 
       or from exiftool (while creating json_file and fixing missing metadata)"""
    if path is None:
        if config.AIOLI_MODE:
            path = config.AIOLI_ACQUISITION_DIR
        else:
            path = '.'

    if os.path.isfile(json_file):
        with open(json_file) as tr:
            return json.load(tr)
    
    emulsion.started('metadata')
    try:
        # lit les métadonnées
        jdump = subprocess.check_output(f'exiftool -FileName -ImageWidth \
            -ImageHeight -FocalLengthIn35mmFormat -FocalLength -FocalLength35efl \
            -Make -Model -ISO -ISOSetting -ISO2 -FNumber -WhiteBalance \
            -ExposureMode -ExposureProgram -Orientation -CameraOrientation \
            -DateTimeOriginal -DateCreated -DateTimeDigitized -DigitalCreationDate\
            -DateTime -Rotation -json --printConv {path}', shell=True).decode('utf-8')
        
        dump = json.loads(jdump)
        
        # les écrit dans un json
        with open(json_file, 'w+') as jf:
            jf.write(jdump)
        
        # TODO plutôt créer LocalChantierDescripteur ??
        # modifie les métadonnées si nulle ou inexistantes (focale et focale 35mm)
        if auto_fix:
            subprocess.run(f"exiftool \
            -if '$focallength# eq 0' -FocalLength=35 -execute \
            -if 'not $focallength'   -FocalLength=35 -execute \
            -if '$focallengthin35mmformat# eq 0' -FocalLengthIn35mmFormat=35 -execute \
            -if 'not $focallengthin35mmformat'   -FocalLengthIn35mmFormat=35 -execute \
            -common_args -overwrite_original {path}", shell=True, stdout=DEVNULL)
        
    except:
        emulsion.failed()
        raise
    
    emulsion.finished()
    
    return dump 

def load_dataset(dump):
    """returns full resolution and set of fisheye images from input metadata dict list"""
    mode_fisheye = []
    image_max_dimension = 0
    image_list = []

    for meta in dump:
        logger.debug(meta)
        try:
            foc35 = meta['FocalLengthIn35mmFormat']
        except KeyError:
            foc35 = 35 # this is the value written by metadata.sh
        
        image_list.append(meta['FileName'])
        if foc35 <= 15 and foc35 != 0:
            mode_fisheye.append(meta['FileName'])

        with suppress(KeyError):
            image_max_dimension = max(
                image_max_dimension, meta['ImageWidth'], meta['ImageHeight']
            )
    return image_list, image_max_dimension, mode_fisheye

def top_homol_matches(image, suffix, n, ext='.txt'):
    """returns array of filenames with n best matches"""
    matches = []
    
    """
    for f in os.scandir('Homol%s/Pastis%s' % (suffix, image)):
        if f.name.endswith('.txt'):
            im = os.path.splitext(f.name)[0]
            size = f.stat().st_size 
            matches.append((im, size))
    # other way around to find (more?) matches from Tapioca
    # ...but need to reverse columns for Im2XYZ (x0, y0, x1, y1) -> (x1, y1, x0, y0)
    """
    p = pathlib.Path(f'Homol{suffix}')
    for f in p.glob(f'Pastis*/{image}{ext}'):
        im = f.parts[1][6:] # get folder name with "Pastis" stripped
        size = f.stat().st_size
        matches.append((im, size))

    matches.sort(key=lambda x: x[1], reverse=True)

    return matches[:n]

if __name__ == "__main__":
    print("use initial.py, incremental.py or resection.py")

#!/usr/bin/env python3
""" Script pour l'itération initiale """
import sys
import os
import logging
import argparse
import pathlib
from shutil import rmtree, copytree, copy
from contextlib import suppress

from culbu3 import culbuto
import config
import emulsion
import taco2 as taco

logger = logging.getLogger()

def initial_main(fpattern, ori, manioc_level, run_culbuto=False, skip_tapioca=False):
    """processes an initial acquisition"""
    # lit (ou écrit à partir des images) un fichier 0.json (métadonnées)
    dump = taco.process_metadata('0.json')
    # récupére la liste image, résolution max et listes images fisheye
    image_list, res_full, fisheye_mode = taco.load_dataset(dump)

    logger.debug(res_full)

    if run_culbuto:
        logger.info('running culbuto...')
        try:
            emulsion.started('culbuto_scan')
            culbuto(image_list, 'Culbuto-S2D.xml', 'Culbuto-S3D.xml')
            emulsion.finished()
        except:
            emulsion.failed()

    manioc = None
    if not skip_tapioca:
        if fisheye_mode:
            logger.debug('fisheye : %s', ', '.join(fisheye_mode))
            manioc_level += 1 # saute la première séquence tapioca mulscale (500 1000)
        
        manioc = taco.Manioc(fpattern, res_full, manioc_level)
        manioc.run()

    try:
        taco.antipasti(fpattern, ori, fisheye=fisheye_mode, run_culbuto=run_culbuto, 
            do_c3dc=True, manioc=manioc)
    except taco.MicMacError as e:
        logger.critical(e)
        sys.exit(1)

    logger.info('done!')

def cli():
    """entry-point for cli"""
    default_cpath = '.'
    if config.AIOLI_MODE:
        default_cpath = config.AIOLI_CHANTIER_DIR

    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('initial_pattern', nargs='?', default='ITER0+__.+')
    parser.add_argument("-c", "--chantier-path", type=pathlib.Path, default=default_cpath)
    parser.add_argument("-m", "--manioc-level", action="count", default=0)
    parser.add_argument("--skip-tapioca", action='store_true')
    parser.add_argument("--culbuto", action='store_true')
    #parser.add_argument("--c3dc-options", type=str, nargs='+')
    parser.add_argument("--process-id", help="Process ID pour Aïoli", type=emulsion.set_process_id)

    args = parser.parse_args()

    path = args.chantier_path.expanduser()
    os.chdir(str(path))

    fp = args.initial_pattern
    
    if not config.AIOLI_MODE:
        if fp == 'ITER0+__.+': # si filepattern par défaut
            if not list(path.glob('ITER0__?*')): # ... et pas de fichier commençant par ITER0__
                for f in path.iterdir(): # ... renommer tout les fichiers en ITER0__
                    if f.is_file():
                        os.rename(str(f), 'ITER0__' + str(f))

    initial_main(fp, 'All', 
        args.manioc_level, 
        run_culbuto=args.culbuto,
        skip_tapioca=args.skip_tapioca)
    
    if config.AIOLI_MODE:
        # pour viewer ND
        taco.micmac('OriExport', 'Ori-All/Orientation-.*.xml', 'export.txt')
        # pour indexation Adeline
        for orientation in path.glob('Ori-All/Orientation-*.xml'):
            taco.micmac('GenerateBorderCam', str(orientation), '1')

    # cleanup
    rmtree("NewOriTmpQuick", ignore_errors=True)
    rmtree("Ori-InterneScan", ignore_errors=True)

if __name__ == "__main__":
    taco.configure_log_and_errors(logger, 'taco-initial')
    cli()
